#!/bin/bash

# Run tmux list-keys and save the output
keys=$(tmux list-keys)

# Parse the output into CSV format
csv=$(echo "$keys" | awk '{print "\"" $1 "\",\"" $2 "\",\"" $3 "\",\"" $4 "\""}')

# Convert the CSV into a markdown table
table=$(echo "$csv" | sed -e '1i\
"Command","Key","Context","Action"
' | sed 's/,/ | /g' | sed 's/^/| /' | sed 's/$/ |/' | sed -e '2i\
|:---------:|:-------:|:-----:|:-------------:|
')

# Save the table to a markdown file
echo "$table" >> ~/Documents/3_Resources/Utils/tmux_keys.md
#!/bin/bash

# Run tmux list-keys and save the output
keys=$(tmux list-keys)

# Parse the output into CSV format
csv=$(echo "$keys" | awk '{print "\"" $1 "\",\"" $2 "\",\"" $3 "\",\"" $4 "\""}')

# Convert the CSV into a markdown table
table=$(echo "$csv" | sed -e '1i\
"Command","Key","Context","Action"
' | sed 's/,/ | /g' | sed 's/^/| /' | sed 's/$/ |/' | sed -e '2i\
|:---------:|:-------:|:-----:|:-------------:|
')

# Save the table to a markdown file
echo "$table" >> ~/Documents/3_Resources/Utils/tmux_keys.md

